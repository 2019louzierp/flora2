# Rapport

## 1 - Présentation de Flora-2

Flora-2 est un système open-source orienté objet basé sur des règles sémantiques pour la représentation et le raisonnement des connaissances. Le langage utilisé par le système est dérivé de F-Logic, Hilog et Transaction Logic.

Parmi les applications de Flora-2 on trouve : les agents intelligents, le Web Semantic, la gestion d'ontologie, etc.

Etant donné que Flora-2 est basé sur de la F-Logic, ce système de représentation est à la fois un langage robuste de modélisation conceptuelle et propose une syntaxe déclarative compacte, simple et précise.

### Comparaison OWL et Flora-2

Flora-2 intègre un raisonneur qui permet de faire des règles plus complexes que OWL et d'en déduire de nouvelles à partir de règles existantes.

Un autre avantage de Flora-2 est sa résilience face au changement. Il est possible de modifier beaucoup plus faiclement des faits ou des règles de manière dynamique. Cela facilite grandement la gestion des ontologies.

Non monotonie : Flora-2 est capable de gérer le cas où deux règles débouchent sur un résultat contradictoire en instaurant des principes de priorité sur ses règles.

Nous mettrons en évidence ces spécifictés dans la présentation qui suit.

## 2 - Exemple d'application

Nous utiliserons des exemples de CentraleSupélec pour montrer les specificités de Flora-2.

### Classes

Nous définissons la classe `Student` qui contient des informations propres aux étudiants de CentraleSupélec.

```flora2
Student[|
    Joined_In => \integer,
    Graduated_In => \integer,
    Major => \string,
    Mean_Grade => \integer,
    Pass => \boolean,
    has_grade(Course) => Grade
|].
```

Définissons ensuite la classe `Teacher` qui enregistre les informations des professeurs.

```flora2
Teacher [|
    Domain => \string,
    Joined_In => \integer
|].
```

Les deux classes héritent de la classe `Person`.

```flora2
Person [|
    First_name => \string,
    Last_name => \string
|].

Teacher :: Person
Student :: Person
```

Les classes `Course` représentent les cours. Ils sont suivis par les étudiants et enseignés par les professeurs. La classe `Grade` contient les notes des élèves dans les cours.

```flora2
Course[|
    Name => string,
    Number_Students => \integer
|].

Grade[|
    Number => \integer,
    Domain => Course,
    Student => Student
|].
```

### Instances

Nous créons des instances pour les différentes classes.

Pour les classes `Student` et `Teacher`:

```flora2
Paul : Student[
    First_name -> 'Paul',
    Last_name -> 'Louzier',
    Joined_In -> 2019,
    took -> {CR, SA, GPT}
].

Julien : Student[
    First_name -> 'Julien',
    Last_name -> 'Pinede',
    Joined_In -> 2019,
    took -> {SA}
].

FP : Teacher[
    First_name -> 'Fabrice',
    Last_name -> 'Popineau',
    Domain -> 'Artificial Intelligence',
    Joined_In -> 1991,
    teaches -> CR
].
```

Pour les classes `Course`:

```flora2
CR : Course[
    Name -> 'Connaissances et Raisonnement',
    Domain -> 'Artificial Intelligence'
].
SA : Course[
    Name -> 'Statistiques et Apprentissage',
    Domain -> 'Mathematics'
].
GPT : Course[
    Name -> 'ChatGPT',
    Domain -> 'Artificial Intelligence'
].
```

### Règles

Toutes les manipulations précédentes sont réalisables facilement sur OWL. Cependant, Flora-2 permet l'ajout de règles, ce qui peut faciliter la récupération des données et permettre d'exécuter des Query complexes facilement, notamment lorsque de la logique mathématique est appliquée.

#### Règles statiques

On commence par définir les différentes règles de base de nos données.

En premier lieu, on considère que les élèves ne peuvent pas être des professeurs et vice-versa:

```flora2
@{disjoint} \neg ?X:Student :- ?X:Teacher
```

De plus, étant donné qu'il est fastidieux de renseigner manuellement les années de sortie des étudiants, nous établissons une règle qui nous permet de la déterminer à partir de leur année d'entrée.

```flora2
?X[Graduated_In -> ?Y + 3] :- ?X[Joined_In -> ?Y].
```

De cette façon on obtient facilement l'année de diplomation d'un élève:

```flora2
flora2 ?- Paul[Graduated_In -> ?Y].

?Y = 2019 + 3

1 solution(s) in 0.000 seconds; elapsed time = 0.000

Yes
```

Le résultat renvoyé, `2019 + 3` n'est pas très agréable à la lecture, nous utilisons donc une deuxième spécificité de Flora-2, les fonctions, pour définir la somme de deux nombres.

```flora2
\udf sum(?x, ?y) := ?z \if ?z \is ?x + ?y.

?X[Graduated_In -> sum(?Y, 3)] :- ?X[Joined_In -> ?Y].
```

Alors nous retrouvons un résultat plus clair.

```flora2
flora2 ?- Paul[Graduated_In -> ?Y].

?Y = 2022

1 solution(s) in 0.000 seconds; elapsed time = 0.000

Yes
```

De même que pour l'année de sortie, nous pouvons calculer la moyenne d'un élève automatiquement:

```flora2
@{average_grade} ?X[Mean_Grade -> ?avgGrade] :-
    ?avgGrade = avg{?N | ?_G:Grade[Number -> ?N, Student -> ?X]}.
```

```flora2
flora2 ?- Paul[Mean_Grade -> ?N].

?N = 9.33333333333333

1 solution(s) in 0.000 seconds; elapsed time = 0.001

Yes
```

#### Méthodes

Nous pouvons aussi utiliser les règles pour définir des méthodes de classe.
Nous définissons les méthodes `has_grade` et `professor` qui nous permettent respectivement de retrouver les notes d'un élève dans une matière et de lister les professeurs qui enseignent les cours qu'il suit.

```flora2
@{professor_method} ?S[professor(?Course) -> ?T] :- ?T[teaches -> ?Course], ?S[took -> ?Course].

@{has_grade1} ?S[has_grade(?Course) -> ?G] :- ?G:Grade[Domain -> ?Course, Student -> ?S].
@{has_grade2} ?G[Number -> ?Number, Domain -> ?Course, Student -> ?S] :-
    ?G:Grade[Number -> ?Number], ?S:Student[has_grade(?Course) -> ?G].
```

Alors avec les données suivantes

```flora2
Paul[has_grade(SA) -> G1:Grade[Number -> 8]].
Paul[has_grade(CR) -> G2:Grade[Number -> 20]].
Paul[has_grade(GPT) -> G3:Grade[Number -> 0]].
```

Nous pouvons obtenir les réponses correctes à nos queries

```flora2
flora2 ?- Paul[professor(CR) -> ?P].

?P = FP

1 solution(s) in 0.016 seconds; elapsed time = 0.000

Yes
```

```flora2
flora2 ?- ?G:Grade[Number -> ?N, Domain -> ?D, Student -> ?S].

?G = G1
?N = 8
?D = SA
?S = Paul

?G = G2
?N = 20
?D = CR
?S = Paul

?G = G3
?N = 0
?D = GPT
?S = Paul

3 solution(s) in 0.000 seconds; elapsed time = 0.001

Yes
```

### Utilisation dynamique

Imaginons à présent que l'application décrite plus haut soit utilisé par l'administration de CentraleSupélec. Après quelques temps d'utilisation, ils souhaitent savoir si un élève est en mesure de valider son année. Pour cela, il souhaitent ajouter dynamiquement des règles à l'application.
Flora-2 est capable de gérer ces cas puisqu'il permet de modifier les règles de manière dynamique, ce qui évite aux responsables de la DISI de devoir modifier le fichier source. Pour cela, on ajoute la commande `insertrule`.

```flora2
flora2 ?- insertrule{Student[| pass => \boolean |]}.
flora2 ?- insertrule{@{pass_true} ?X[Pass -> true] :- ?M >= 10, ?X[Mean_Grade -> ?M]}.
flora2 ?- insertrule{?X[Pass -> false] :- ?M < 10, ?X[Mean_Grade -> ?M]}.
```

```flora2
flora2 ?- Paul[Pass -> ?P].

?P = false

1 solution(s) in 0.000 seconds; elapsed time = 0.000

Yes
```

Cependant, si plus tard le règlement des études est modifié et que la tolérance pour valider une année est de 9, il est possible de supprimer puis rajouter des règles de façon dynamique.

```flora2
flora2 ?- deleterule{?X[Pass -> true] :- ?M >= 10, ?X[Mean_Grade -> ?M]}.
flora2 ?- deleterule{?X[Pass -> false] :- ?M < 10, ?X[Mean_Grade -> ?M]}.
flora2 ?- insertrule{?X[Pass -> true] :- ?M >= 9, ?X[Mean_Grade -> ?M]}.
flora2 ?- insertrule{?X[Pass -> false] :- ?M < 9, ?X[Mean_Grade -> ?M]}.
```

```flora2
flora2 ?- Paul[Pass -> ?P].

?P = true

1 solution(s) in 0.000 seconds; elapsed time = 0.000

Yes
```

Il est aussi possible de désactiver une règle statique. Ceci permettra de la rendre muette tout en la conservant afin de pouvoir la réactiver.

L'administration décide de tester une nouvelle idée : les élèves peuvent devenir teaching assistant. Il va donc falloir disable la règle statique qui affirme la disjonction des classes Student et Teacher.

```flora2
flora2 ?-disable{disjoint,\@F,\@M}

```

Si l'expérience n'est pas concluante et qu'on préfère interdire le fait qu'un élève soit professeur on peut réactiver la règle de nouveau et interdire aux élèves d'être professeur

```flora2
flora2 ?-enable{disjoint,\@F,\@M}
```

Enfin, nous pouvons imaginer le cas où l'administration souhaite prendre en compte les doubles diplômes pour calculer l'année de diplômation. En effet les étudiants effectuant un double diplôme dans une école extérieure reçoivent leur diplôme 4 ans après leur entrée en école et non 3 ans après comme la plupart des autres élèves. Dans ce cas l'administration choisit de rajouter quelques éléments dans l'application:

```flora2
flora2 ?- insert{Student[| Double_Graduate => \boolean |]}.
flora2 ?- insert{Julien[Double_Graduate -> true]}.
flora2 ?- insertrule{@{dual_degree} ?X[Graduated_In -> sum(?Y, 4)] :- ?X[Joined_In -> ?Y, Double_Graduate -> true]}.
```

Malheureusement dans ce cas Julien n'a pas de date de diplomation définie.

```flora2
flora2 ?- Julien[Graduated_In -> ?Y].

?Y = 2022

?Y = 2023

2 solution(s) in 0.000 seconds; elapsed time = 0.000

Yes
```

Nous utilisons donc la priorisation mise à disposition par Flora-2 pour exprimer quelle règle prévaut

```flora2
flora2 ?- insertrule{\overrides(dual_degree, graduated_rule)}.
flora2 ?- insertrule{\overrides({?X[Graduated_In -> sum(?Y, 4)] :- ?X[Joined_In -> ?Y, Double_Graduate -> true]}, {?X[Graduated_In -> sum(?Y, 3)] :- ?X[Joined_In -> ?Y]})}.
```

De cette façon, Julien a bien l'année de graduation demandée tandis que Paul a une sortie correcte.

```flora2
flora2 ?- Julien[Graduated_In -> ?Y].

?Y = 2023

1 solution(s) in 0.000 seconds; elapsed time = 0.000

Yes
```

```flora2
flora2 ?- Paul[Graduated_In -> ?Y].

?Y = 2022

1 solution(s) in 0.000 seconds; elapsed time = 0.000

Yes
```

## 3 - Conclusion

Ce projet nous a permis d'approfondir le cours en découvrant un outil alternatif et à la fois plus robuste et plus souple pour l'élaboration d'ontologies munies de règles dynamiques. La mise en place d'une application élémentaire a facilité notre prise en main et notre compréhension de l'outil. Cependant, son manque de documentation et d'actualisation peut poser des difficultés pour son appréhesion pour de nouveaux utilisateurs.
