
:-(compiler_options([xpp_on,canonical])).

/********** Tabling and Trailer Control Variables ************/

#define EQUALITYnone
#define INHERITANCEflogic
#define TABLINGreactive
#define TABLINGvariant
#define CUSTOMnone

#define FLORA_INCREMENTAL_TABLING 

/************************************************************************
  file: headerinc/flrheader_inc.flh

  Author(s): Guizhen Yang

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).
#mode standard Prolog

#include "flrheader.flh"
#include "flora_porting.flh"

/***********************************************************************/

/************************************************************************
  file: headerinc/flrheader_prog_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).
#mode standard Prolog

#include "flrheader_prog.flh"

/***********************************************************************/

#define FLORA_COMPILATION_ID 36

#mode save
#mode nocomment "%"
#define FLORA_FLT_FILENAME "cs.flt"
#mode restore
/************************************************************************
  file: headerinc/flrheader2_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the Flora-2 compiler.
  It has files that must be included in the header and typically
  contain some Prolog statements. Such files cannot appear
  in flrheader.flh because flrheader.flh is included in various restricted
  contexts where Prolog statements are not allowed.

  NOT included in ADDED files (compiled for addition) -- only in LOADED
  ones and in trailers/patch
************************************************************************/

:-(compiler_options([xpp_on])).

#define TABLING_CONNECTIVE  :-

%% flora_tabling_methods is included here to affect preprocessing of
%% flrtable/flrhilogtable.flh dynamically
#include "flora_tabling_methods.flh"

/* note: inside flrtable.flh there are checks for FLORA_NONTABLED_DATA_MODULE
   that exclude tabling non-signature molecules
*/
#ifndef FLORA_NONTABLED_MODULE
#include "flrtable.flh"
#endif

/* if normal tabled module, then table hilog */
#if !defined(FLORA_NONTABLED_DATA_MODULE) && !defined(FLORA_NONTABLED_MODULE)
#include "flrhilogtable.flh"
#endif

#include "flrtable_always.flh"

#include "flrauxtables.flh"

%% include list of tabled predicates
#mode save
#mode nocomment "%"
#if defined(FLORA_FLT_FILENAME)
#include FLORA_FLT_FILENAME
#endif
#mode restore

/***********************************************************************/

/************************************************************************
  file: headerinc/flrdyna_inc.flh

  Author(s): Chang Zhao

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).

#define TABLING_CONNECTIVE  :-

#include "flrdyndeclare.flh"

/***********************************************************************/

/************************************************************************
  file: headerinc/flrindex_P_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).

#include "flrindex_P.flh"

/***********************************************************************/

#mode save
#mode nocomment "%"
#define FLORA_THIS_FILENAME  'cs.flr'
#mode restore
/************************************************************************
  file: headerinc/flrdefinition_inc.flh

  Author(s): Guizhen Yang

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

#include "flrdefinition.flh"

/***********************************************************************/

/************************************************************************
  file: headerinc/flrtrailerregistry_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

#include "flrtrailerregistry.flh"

/***********************************************************************/

/************************************************************************
  file: headerinc/flrrefreshtable_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).

#include "flrrefreshtable.flh"

/***********************************************************************/

/************************************************************************
  file: headerinc/flrdynamic_connectors_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).

#include "flrdynamic_connectors.flh"

/***********************************************************************/

/************************************************************************
  file: syslibinc/flrimportedcalls_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the FLORA-2 compiler.
************************************************************************/

%% Loads the file with all the import statements for predicates
%% that must be known everywhere

:-(compiler_options([xpp_on])).

#mode standard Prolog

#if !defined(FLORA_TERMS_FLH)
#define FLORA_TERMS_FLH
#include "flora_terms.flh"
#endif

?-(:(flrlibman,flora_load_library(FLLIBIMPORTEDCALLS))).

/***********************************************************************/

/************************************************************************
  file: headerinc/flrpatch_inc.flh

  Author(s): Guizhen Yang

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

#include "flrexportcheck.flh"
#include "flrpatch.flh"
/***********************************************************************/

/************************************************************************
  file: headerinc/flropposes_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

#include "flropposes.flh"

/***********************************************************************/

/************************************************************************
  file: headerinc/flrhead_dispatch_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).

#include "flrhead_dispatch.flh"

/***********************************************************************/

/************************************************************************
  file: syslibinc/flraggavg_inc.flh

  Author(s): Guizhen Yang

  This file is automatically included by the FLORA-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).

#mode standard Prolog

#if !defined(FLORA_TERMS_FLH)
#define FLORA_TERMS_FLH
#include "flora_terms.flh"
#endif

?-(:(flrlibman,flora_load_library(FLLIBAVG))).

/***********************************************************************/

/************************************************************************
  file: syslibinc/flrclause_inc.flh

  Author(s): Chang Zhao

  This file is automatically included by the FLORA-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).

#mode standard Prolog

#if !defined(FLORA_TERMS_FLH)
#define FLORA_TERMS_FLH
#include "flora_terms.flh"
#endif

?-(:(flrlibman,flora_load_library(FLLIBCLAUSE))).

/***********************************************************************/

/************************************************************************
  file: ATinc/flrgclp_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the FLORA-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).

#mode standard Prolog

#if !defined(FLORA_TERMS_FLH)
#define FLORA_TERMS_FLH
#include "flora_terms.flh"
#endif

?-(:(flrlibman,flora_load_system_module(FLSYSMODGCLP))).

/***********************************************************************/

:-(dynamic(as(','(/(FLORA_THIS_WORKSPACE(FLDYNAPREFIX_UNQ(new_udf_predicate_sum)),4),/(FLORA_THIS_WORKSPACE(FLDYNZPREFIX_UNQ(new_udf_predicate_sum)),4)),opaque))).
:-(FLORA_THIS_WORKSPACE(new_udf_predicate_sum)(_h166287,_h166289,_h166291,FWContext),FLORA_THIS_WORKSPACE(FLDYNAPREFIX_UNQ(new_udf_predicate_sum))(_h166287,_h166289,_h166291,FWContext)).
 
#if !defined(FLORA_FDB_FILENAME)
#if !defined(FLORA_LOADDYN_DATA)
#define FLORA_LOADDYN_DATA
#endif
#mode save
#mode nocomment "%"
#define FLORA_FDB_FILENAME  'cs.fdb'
#mode restore
?-(:(flrutils,flora_loaddyn_data(FLORA_FDB_FILENAME,FLORA_THIS_MODULE_NAME,'fdb'))).
#else
#if !defined(FLORA_READ_CANONICAL_AND_INSERT)
#define FLORA_READ_CANONICAL_AND_INSERT
#endif
?-(:(flrutils,flora_read_canonical_and_insert(FLORA_FDB_FILENAME,FLORA_THIS_FDB_STORAGE))).
#endif

 
#if !defined(FLORA_FLM_FILENAME)
#if !defined(FLORA_LOADDYN_DATA)
#define FLORA_LOADDYN_DATA
#endif
#mode save
#mode nocomment "%"
#define FLORA_FLM_FILENAME  'cs.flm'
#mode restore
?-(:(flrutils,flora_loaddyn_data(FLORA_FLM_FILENAME,FLORA_THIS_MODULE_NAME,'flm'))).
#else
#if !defined(FLORA_READ_CANONICAL_AND_INSERT)
#define FLORA_READ_CANONICAL_AND_INSERT
#endif
?-(:(flrutils,flora_read_descriptor_metafacts_canonical_and_insert(cs,_ErrNum))).
#endif

 
#if !defined(FLORA_FLD_FILENAME)
#if !defined(FLORA_LOADDYN_DATA)
#define FLORA_LOADDYN_DATA
#endif
#mode save
#mode nocomment "%"
#define FLORA_FLD_FILENAME  'cs.fld'
#mode restore
?-(:(flrutils,flora_loaddyn_data(FLORA_FLD_FILENAME,FLORA_THIS_MODULE_NAME,'fld'))).
#else
#if !defined(FLORA_READ_CANONICAL_AND_INSERT)
#define FLORA_READ_CANONICAL_AND_INSERT
#endif
?-(:(flrutils,flora_read_canonical_and_insert(FLORA_FLD_FILENAME,FLORA_THIS_FLD_STORAGE))).
#endif

 
#if !defined(FLORA_FLS_FILENAME)
#if !defined(FLORA_LOADDYN_DATA)
#define FLORA_LOADDYN_DATA
#endif
#mode save
#mode nocomment "%"
#define FLORA_FLS_FILENAME  'cs.fls'
#mode restore
?-(:(flrutils,flora_loaddyn_data(FLORA_FLS_FILENAME,FLORA_THIS_MODULE_NAME,'fls'))).
#else
#if !defined(FLORA_READ_CANONICAL_AND_INSERT)
#define FLORA_READ_CANONICAL_AND_INSERT
#endif
?-(:(flrutils,flora_read_symbols_canonical_and_insert(FLORA_FLS_FILENAME,FLORA_THIS_FLS_STORAGE,_SymbolErrNum))).
#endif


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Rules %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-(import(from(','(/(add_to_flora_registry,1),/(flora_defeasible_module_registry,2)),flrregistry))).
?-(add_to_flora_registry(flora_defeasible_module_registry(FLORA_THIS_MODULE_NAME,'\\gcl'))).
#define FLORA_DEFEASIBLE_THEORY 
#include flrdefeasible.flh 
?-(assert(flora_defined_udf_registry(FLORA_THIS_MODULE_NAME,sum,2,flapply(sum,__x,__y),FLORA_THIS_WORKSPACE(new_udf_predicate_sum),FLORA_THIS_WORKSPACE(new_udf_predicate_sum)(__newvar1,flapply(sum,__x,__y),__newdontcarevar2)))).
?-(assert(flora_used_udf_registry(sum,2,flapply(sum,__x,__y),FLORA_THIS_WORKSPACE(new_udf_predicate_sum)(__newvar1,flapply(sum,__x,__y),__newdontcarevar2)))).
:-(FLORA_THIS_WORKSPACE(new_udf_predicate_sum)(__newvar1,flapply(sum,__x,__y),'_$ctxt'(_CallerModuleVar,4,__newcontextvar3)),','(','(fllibdelayedliteral('\\is','cs.flr',39,[__z,+(__x,__y)]),=(__newvar1,__z)),fllibexecute_delayed_calls([__newvar1,__x,__y,__z],[__newvar1,__x,__y]))).
:-(FLORA_THIS_WORKSPACE(static^mvd)(__X,'Graduated_In',__newvar1,'_$ctxt'(_CallerModuleVar,6,__newcontextvar2)),','('_$_$_flora''rule_enabled'(6,'cs.flr',FLORA_THIS_MODULE_NAME),','('_$_$_flora''udf_hilog_predicate'(flapply(sum,__Y,3),2,FLORA_THIS_MODULE_NAME,__newvar1),','(FLORA_THIS_WORKSPACE(d^mvd)(__X,'Joined_In',__Y,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,6)),flibdefeatdelay('cs.flr',43,FLORA_WORKSPACE(\\gcl,\\undefeated)(6,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_WORKSPACE(d^mvd)(__X,'Graduated_In',__newvar1,'_$ctxt'(_CallerModuleVar,6,__newcontextvar2)),FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,__newcontextvar5)),null,[__X,__newvar1],['X',newvar1]))))).
:-(FLORA_THIS_WORKSPACE(static^neg^isa)(__X,'Teacher','_$ctxt'(_CallerModuleVar,8,__newcontextvar1)),','('_$_$_flora''rule_enabled'(8,'cs.flr',FLORA_THIS_MODULE_NAME),','(FLORA_THIS_WORKSPACE(d^isa)(__X,'Student','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,8)),flibdefeatdelay('cs.flr',44,FLORA_WORKSPACE(\\gcl,\\undefeated)(8,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_WORKSPACE(neg^d^isa)(__X,'Teacher','_$ctxt'(_CallerModuleVar,8,__newcontextvar1)),FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,__newcontextvar4)),null,[__X],['X'])))).
:-(FLORA_THIS_WORKSPACE(static^mvd)(__X,'Mean_Grade',__avgGrade,'_$ctxt'(_CallerModuleVar,10,__newcontextvar1)),','('_$_$_flora''rule_enabled'(10,'cs.flr',FLORA_THIS_MODULE_NAME),','(','(fllibavg(__newdontcarevar5,[],[],','(','(','(FLORA_THIS_WORKSPACE(d^mvd)(___G,'Number',__newdontcarevar5,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,10)),FLORA_THIS_WORKSPACE(d^mvd)(___G,'Student',__X,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,10))),FLORA_THIS_WORKSPACE(d^isa)(___G,'Grade','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,10))),fllibexecute_delayed_calls([__newdontcarevar5,__X,___G],[])),__newvar6),=(__avgGrade,__newvar6)),','(flibdefeatdelay('cs.flr',45,FLORA_WORKSPACE(\\gcl,\\undefeated)(10,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_WORKSPACE(d^mvd)(__X,'Mean_Grade',__avgGrade,'_$ctxt'(_CallerModuleVar,10,__newcontextvar1)),FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar7,__newcontextvar8)),null,[__X,__avgGrade],['X',avgGrade]),fllibexecute_delayed_calls([__X,___G,__avgGrade],[__X,__avgGrade]))))).
:-(FLORA_THIS_WORKSPACE(static^mvd)(__X,'Pass',true,'_$ctxt'(_CallerModuleVar,12,__newcontextvar1)),','('_$_$_flora''rule_enabled'(12,'cs.flr',FLORA_THIS_MODULE_NAME),','(','(fllibdelayedliteral(>=,'cs.flr',47,[__M,10]),FLORA_THIS_WORKSPACE(d^mvd)(__X,'Mean_Grade',__M,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,12))),','(flibdefeatdelay('cs.flr',47,FLORA_WORKSPACE(\\gcl,\\undefeated)(12,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_WORKSPACE(d^mvd)(__X,'Pass',true,'_$ctxt'(_CallerModuleVar,12,__newcontextvar1)),FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,__newcontextvar4)),null,[__X],['X']),fllibexecute_delayed_calls([__M,__X],[__X]))))).
:-(FLORA_THIS_WORKSPACE(static^mvd)(__X,'Pass',false,'_$ctxt'(_CallerModuleVar,14,__newcontextvar1)),','('_$_$_flora''rule_enabled'(14,'cs.flr',FLORA_THIS_MODULE_NAME),','(','(fllibdelayedliteral(<,'cs.flr',48,[__M,10]),FLORA_THIS_WORKSPACE(d^mvd)(__X,'Mean_Grade',__M,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,14))),','(flibdefeatdelay('cs.flr',48,FLORA_WORKSPACE(\\gcl,\\undefeated)(14,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_WORKSPACE(d^mvd)(__X,'Pass',false,'_$ctxt'(_CallerModuleVar,14,__newcontextvar1)),FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,__newcontextvar4)),null,[__X],['X']),fllibexecute_delayed_calls([__M,__X],[__X]))))).
:-(FLORA_THIS_WORKSPACE(static^mvd)(__S,flapply(professor,__Course),__T,'_$ctxt'(_CallerModuleVar,16,__newcontextvar1)),','('_$_$_flora''rule_enabled'(16,'cs.flr',FLORA_THIS_MODULE_NAME),','(','(FLORA_THIS_WORKSPACE(d^mvd)(__T,teaches,__Course,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,16)),FLORA_THIS_WORKSPACE(d^mvd)(__S,took,__Course,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,16))),flibdefeatdelay('cs.flr',49,FLORA_WORKSPACE(\\gcl,\\undefeated)(16,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_WORKSPACE(d^mvd)(__S,flapply(professor,__Course),__T,'_$ctxt'(_CallerModuleVar,16,__newcontextvar1)),FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,__newcontextvar5)),null,[__S,__Course,__T],['S','Course','T'])))).
:-(FLORA_THIS_WORKSPACE(static^mvd)(__S,flapply(has_grade,__Course),__G,'_$ctxt'(_CallerModuleVar,18,__newcontextvar1)),','('_$_$_flora''rule_enabled'(18,'cs.flr',FLORA_THIS_MODULE_NAME),','(','(','(FLORA_THIS_WORKSPACE(d^mvd)(__G,'Domain',__Course,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,18)),FLORA_THIS_WORKSPACE(d^mvd)(__G,'Student',__S,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,18))),FLORA_THIS_WORKSPACE(d^isa)(__G,'Grade','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,18))),flibdefeatdelay('cs.flr',50,FLORA_WORKSPACE(\\gcl,\\undefeated)(18,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_WORKSPACE(d^mvd)(__S,flapply(has_grade,__Course),__G,'_$ctxt'(_CallerModuleVar,18,__newcontextvar1)),FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar5,__newcontextvar6)),null,[__S,__Course,__G],['S','Course','G'])))).
:-(FLORA_THIS_WORKSPACE(static^mvd)(__G,'Number',__Number,'_$ctxt'(_CallerModuleVar,20,__newcontextvar1)),','('_$_$_flora''rule_enabled'(20,'cs.flr',FLORA_THIS_MODULE_NAME),','(','(','(FLORA_THIS_WORKSPACE(d^mvd)(__G,'Number',__Number,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar5,20)),FLORA_THIS_WORKSPACE(d^isa)(__G,'Grade','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,20))),','(FLORA_THIS_WORKSPACE(d^mvd)(__S,flapply(has_grade,__Course),__G,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar7,20)),FLORA_THIS_WORKSPACE(d^isa)(__S,'Student','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar6,20)))),','(flibdefeatdelay('cs.flr',51,FLORA_WORKSPACE(\\gcl,\\undefeated)(20,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_WORKSPACE(d^mvd)(__G,'Number',__Number,'_$ctxt'(_CallerModuleVar,20,__newcontextvar1)),FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar8,__newcontextvar9)),null,[__G,__Number],['G','Number']),fllibexecute_delayed_calls([__Course,__G,__Number,__S],[__G,__Number]))))).
:-(FLORA_THIS_WORKSPACE(static^mvd)(__G,'Domain',__Course,'_$ctxt'(_CallerModuleVar,20,__newcontextvar2)),','('_$_$_flora''rule_enabled'(20,'cs.flr',FLORA_THIS_MODULE_NAME),','(','(','(FLORA_THIS_WORKSPACE(d^mvd)(__G,'Number',__Number,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar5,20)),FLORA_THIS_WORKSPACE(d^isa)(__G,'Grade','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,20))),','(FLORA_THIS_WORKSPACE(d^mvd)(__S,flapply(has_grade,__Course),__G,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar7,20)),FLORA_THIS_WORKSPACE(d^isa)(__S,'Student','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar6,20)))),','(flibdefeatdelay('cs.flr',51,FLORA_WORKSPACE(\\gcl,\\undefeated)(20,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_WORKSPACE(d^mvd)(__G,'Domain',__Course,'_$ctxt'(_CallerModuleVar,20,__newcontextvar2)),FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar13,__newcontextvar14)),null,[__G,__Course],['G','Course']),fllibexecute_delayed_calls([__Course,__G,__Number,__S],[__Course,__G]))))).
:-(FLORA_THIS_WORKSPACE(static^mvd)(__G,'Student',__S,'_$ctxt'(_CallerModuleVar,20,__newcontextvar3)),','('_$_$_flora''rule_enabled'(20,'cs.flr',FLORA_THIS_MODULE_NAME),','(','(','(FLORA_THIS_WORKSPACE(d^mvd)(__G,'Number',__Number,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar5,20)),FLORA_THIS_WORKSPACE(d^isa)(__G,'Grade','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,20))),','(FLORA_THIS_WORKSPACE(d^mvd)(__S,flapply(has_grade,__Course),__G,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar7,20)),FLORA_THIS_WORKSPACE(d^isa)(__S,'Student','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar6,20)))),','(flibdefeatdelay('cs.flr',51,FLORA_WORKSPACE(\\gcl,\\undefeated)(20,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_WORKSPACE(d^mvd)(__G,'Student',__S,'_$ctxt'(_CallerModuleVar,20,__newcontextvar3)),FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar18,__newcontextvar19)),null,[__G,__S],['G','S']),fllibexecute_delayed_calls([__Course,__G,__Number,__S],[__G,__S]))))).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Rule signatures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

?-(fllibinsrulesig(4,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,11,FLORA_THIS_WORKSPACE(new_udf_predicate_sum)(__newvar1,flapply(sum,__x,__y),'_$ctxt'(_CallerModuleVar,4,__newcontextvar3)),','(fllibdelayedliteral('\\is','cs.flr',39,[__z,+(__x,__y)]),=(__newvar1,__z)),null,true,fllibexecute_delayed_calls([__newvar1,__x,__y,__z],[__newvar1,__x,__y]),true)).
?-(fllibinsrulesig(6,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,12,FLORA_THIS_WORKSPACE(d^mvd)(__X,'Graduated_In',__newvar1,'_$ctxt'(_CallerModuleVar,6,__newcontextvar2)),FLORA_THIS_WORKSPACE(d^mvd)(__X,'Joined_In',__Y,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,6)),'_$_$_flora''udf_hilog_predicate'(flapply(sum,__Y,3),2,FLORA_THIS_MODULE_NAME,__newvar1),'_$_$_flora''rule_enabled'(6,'cs.flr',FLORA_THIS_MODULE_NAME),null,flibdefeatdelay('cs.flr',43,FLORA_WORKSPACE(\\gcl,\\undefeated)(6,dynrule('cs.flr'),'_$_$_flora''descr_vars',__newvar6,FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar7,__newcontextvar8)),__newvar6,[],[]))).
?-(fllibinsrulesig(8,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,13,FLORA_THIS_WORKSPACE(neg^d^isa)(__X,'Teacher','_$ctxt'(_CallerModuleVar,8,__newcontextvar1)),FLORA_THIS_WORKSPACE(d^isa)(__X,'Student','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,8)),null,'_$_$_flora''rule_enabled'(8,'cs.flr',FLORA_THIS_MODULE_NAME),null,flibdefeatdelay('cs.flr',44,FLORA_WORKSPACE(\\gcl,\\undefeated)(8,dynrule('cs.flr'),'_$_$_flora''descr_vars',__newvar5,FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar6,__newcontextvar7)),__newvar5,[],[]))).
?-(fllibinsrulesig(10,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,14,FLORA_THIS_WORKSPACE(d^mvd)(__X,'Mean_Grade',__avgGrade,'_$ctxt'(_CallerModuleVar,10,__newcontextvar1)),','(fllibavg(__newdontcarevar5,[],[],','(','(','(FLORA_THIS_WORKSPACE(d^mvd)(___G,'Number',__newdontcarevar5,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,10)),FLORA_THIS_WORKSPACE(d^mvd)(___G,'Student',__X,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,10))),FLORA_THIS_WORKSPACE(d^isa)(___G,'Grade','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,10))),fllibexecute_delayed_calls([__newdontcarevar5,__X,___G],[])),__newvar6),=(__avgGrade,__newvar6)),null,'_$_$_flora''rule_enabled'(10,'cs.flr',FLORA_THIS_MODULE_NAME),fllibexecute_delayed_calls([__X,___G,__avgGrade],[__X,__avgGrade]),flibdefeatdelay('cs.flr',45,FLORA_WORKSPACE(\\gcl,\\undefeated)(10,dynrule('cs.flr'),'_$_$_flora''descr_vars',__newvar9,FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar10,__newcontextvar11)),__newvar9,[],[]))).
?-(fllibinsrulesig(12,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,15,FLORA_THIS_WORKSPACE(d^mvd)(__X,'Pass',true,'_$ctxt'(_CallerModuleVar,12,__newcontextvar1)),','(fllibdelayedliteral(>=,'cs.flr',47,[__M,10]),FLORA_THIS_WORKSPACE(d^mvd)(__X,'Mean_Grade',__M,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,12))),null,'_$_$_flora''rule_enabled'(12,'cs.flr',FLORA_THIS_MODULE_NAME),fllibexecute_delayed_calls([__M,__X],[__X]),flibdefeatdelay('cs.flr',47,FLORA_WORKSPACE(\\gcl,\\undefeated)(12,dynrule('cs.flr'),'_$_$_flora''descr_vars',__newvar5,FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar6,__newcontextvar7)),__newvar5,[],[]))).
?-(fllibinsrulesig(14,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,16,FLORA_THIS_WORKSPACE(d^mvd)(__X,'Pass',false,'_$ctxt'(_CallerModuleVar,14,__newcontextvar1)),','(fllibdelayedliteral(<,'cs.flr',48,[__M,10]),FLORA_THIS_WORKSPACE(d^mvd)(__X,'Mean_Grade',__M,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,14))),null,'_$_$_flora''rule_enabled'(14,'cs.flr',FLORA_THIS_MODULE_NAME),fllibexecute_delayed_calls([__M,__X],[__X]),flibdefeatdelay('cs.flr',48,FLORA_WORKSPACE(\\gcl,\\undefeated)(14,dynrule('cs.flr'),'_$_$_flora''descr_vars',__newvar5,FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar6,__newcontextvar7)),__newvar5,[],[]))).
?-(fllibinsrulesig(16,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,17,FLORA_THIS_WORKSPACE(d^mvd)(__S,flapply(professor,__Course),__T,'_$ctxt'(_CallerModuleVar,16,__newcontextvar1)),','(FLORA_THIS_WORKSPACE(d^mvd)(__T,teaches,__Course,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,16)),FLORA_THIS_WORKSPACE(d^mvd)(__S,took,__Course,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,16))),null,'_$_$_flora''rule_enabled'(16,'cs.flr',FLORA_THIS_MODULE_NAME),null,flibdefeatdelay('cs.flr',49,FLORA_WORKSPACE(\\gcl,\\undefeated)(16,dynrule('cs.flr'),'_$_$_flora''descr_vars',__newvar6,FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar7,__newcontextvar8)),__newvar6,[],[]))).
?-(fllibinsrulesig(18,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,18,FLORA_THIS_WORKSPACE(d^mvd)(__S,flapply(has_grade,__Course),__G,'_$ctxt'(_CallerModuleVar,18,__newcontextvar1)),','(','(FLORA_THIS_WORKSPACE(d^mvd)(__G,'Domain',__Course,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar3,18)),FLORA_THIS_WORKSPACE(d^mvd)(__G,'Student',__S,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,18))),FLORA_THIS_WORKSPACE(d^isa)(__G,'Grade','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar2,18))),null,'_$_$_flora''rule_enabled'(18,'cs.flr',FLORA_THIS_MODULE_NAME),null,flibdefeatdelay('cs.flr',50,FLORA_WORKSPACE(\\gcl,\\undefeated)(18,dynrule('cs.flr'),'_$_$_flora''descr_vars',__newvar7,FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar8,__newcontextvar9)),__newvar7,[],[]))).
?-(fllibinsrulesig(20,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,19,FLORA_THIS_WORKSPACE(d^mvd)(__G,'Number',__Number,'_$ctxt'(_CallerModuleVar,20,__newcontextvar1)),','(','(FLORA_THIS_WORKSPACE(d^mvd)(__G,'Number',__Number,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar5,20)),FLORA_THIS_WORKSPACE(d^isa)(__G,'Grade','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,20))),','(FLORA_THIS_WORKSPACE(d^mvd)(__S,flapply(has_grade,__Course),__G,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar7,20)),FLORA_THIS_WORKSPACE(d^isa)(__S,'Student','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar6,20)))),null,'_$_$_flora''rule_enabled'(20,'cs.flr',FLORA_THIS_MODULE_NAME),fllibexecute_delayed_calls([__Course,__G,__Number,__S],[__G,__Number]),flibdefeatdelay('cs.flr',51,FLORA_WORKSPACE(\\gcl,\\undefeated)(20,dynrule('cs.flr'),'_$_$_flora''descr_vars',__newvar10,FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar11,__newcontextvar12)),__newvar10,[],[]))).
?-(fllibinsrulesig(20,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,19,FLORA_THIS_WORKSPACE(d^mvd)(__G,'Domain',__Course,'_$ctxt'(_CallerModuleVar,20,__newcontextvar2)),','(','(FLORA_THIS_WORKSPACE(d^mvd)(__G,'Number',__Number,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar5,20)),FLORA_THIS_WORKSPACE(d^isa)(__G,'Grade','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,20))),','(FLORA_THIS_WORKSPACE(d^mvd)(__S,flapply(has_grade,__Course),__G,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar7,20)),FLORA_THIS_WORKSPACE(d^isa)(__S,'Student','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar6,20)))),null,'_$_$_flora''rule_enabled'(20,'cs.flr',FLORA_THIS_MODULE_NAME),fllibexecute_delayed_calls([__Course,__G,__Number,__S],[__Course,__G]),flibdefeatdelay('cs.flr',51,FLORA_WORKSPACE(\\gcl,\\undefeated)(20,dynrule('cs.flr'),'_$_$_flora''descr_vars',__newvar15,FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar16,__newcontextvar17)),__newvar15,[],[]))).
?-(fllibinsrulesig(20,'cs.flr','_$_$_flora''descr_vars',FLORA_THIS_MODULE_NAME,19,FLORA_THIS_WORKSPACE(d^mvd)(__G,'Student',__S,'_$ctxt'(_CallerModuleVar,20,__newcontextvar3)),','(','(FLORA_THIS_WORKSPACE(d^mvd)(__G,'Number',__Number,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar5,20)),FLORA_THIS_WORKSPACE(d^isa)(__G,'Grade','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar4,20))),','(FLORA_THIS_WORKSPACE(d^mvd)(__S,flapply(has_grade,__Course),__G,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar7,20)),FLORA_THIS_WORKSPACE(d^isa)(__S,'Student','_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar6,20)))),null,'_$_$_flora''rule_enabled'(20,'cs.flr',FLORA_THIS_MODULE_NAME),fllibexecute_delayed_calls([__Course,__G,__Number,__S],[__G,__S]),flibdefeatdelay('cs.flr',51,FLORA_WORKSPACE(\\gcl,\\undefeated)(20,dynrule('cs.flr'),'_$_$_flora''descr_vars',__newvar20,FLORA_THIS_MODULE_NAME,'_$ctxt'(FLORA_THIS_MODULE_NAME,__newcontextvar21,__newcontextvar22)),__newvar20,[],[]))).


%%%%%%%%%%%%%%%%%%%%%%%%% Signatures for latent queries %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%% Queries found in the source file %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


 
#if !defined(FLORA_FLS2_FILENAME)
#if !defined(FLORA_LOADDYN_DATA)
#define FLORA_LOADDYN_DATA
#endif
#mode save
#mode nocomment "%"
#define FLORA_FLS2_FILENAME  'cs.fls2'
#mode restore
?-(:(flrutils,flora_loaddyn_data(FLORA_FLS2_FILENAME,FLORA_THIS_MODULE_NAME,'fls2'))).
#else
#if !defined(FLORA_READ_CANONICAL_AND_INSERT)
#define FLORA_READ_CANONICAL_AND_INSERT
#endif
?-(:(flrutils,flora_read_symbols_canonical_and_insert(FLORA_FLS2_FILENAME,FLORA_THIS_FLS_STORAGE,_SymbolErrNum))).
#endif

/************************************************************************
  file: headerinc/flrtrailer_inc.flh

  Author(s): Michael Kifer

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

#include "flrtrailer.flh"

/***********************************************************************/

/************************************************************************
  file: headerinc/flrpreddef_inc.flh

  Author(s): Chang Zhao

  This file is automatically included by the Flora-2 compiler.
************************************************************************/

:-(compiler_options([xpp_on])).

#include "flrpreddef.flh"

/***********************************************************************/

:-(FLORA_THIS_WORKSPACE(new_udf_predicate_sum)(_h190201,_h190203,_h190205,FWContext),FLORA_THIS_WORKSPACE(FLDYNZPREFIX_UNQ(new_udf_predicate_sum))(_h190201,_h190203,_h190205,FWContext)).
